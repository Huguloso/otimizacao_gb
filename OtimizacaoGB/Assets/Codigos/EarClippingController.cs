using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class EarClippingController : MonoBehaviour
{
    public event Action<Triangle> OnTriangleAdded;
    public event Action<Point> OnPointAdded;
    public event Action<List<Point>> OnPointsAddedToConvexList;
    public event Action<Point> OnPointAddedToEartipList;
    public event Action<Point> OnPointIndexed;
    public event Action<List<Point>> OnPointsSorted;

    [SerializeField] private int _numberOfPoints = 10;
    [SerializeField] private int _pointsRange = 10;

    [SerializeField] private List<Point> _convexPoints = new List<Point>();
    [SerializeField] private List<Point> _earTipPoints = new List<Point>();
    [SerializeField] private List<Point> _pointsList = new List<Point>();

    [SerializeField] private List<Triangle> _trianglesList = new List<Triangle>();

    [SerializeField] private bool _continueLoop;

    private bool _convexPolygon;

    public List<Point> PointsList => _pointsList;

    public List<Point> EarTipPoints => _earTipPoints;

    public List<Point> ConvexPoints => _convexPoints;

    public List<Triangle> TrianglesList => _trianglesList;
    public static Point P0 { get; set; }

    public void CreatePoint(Vector2 position)
    {
        Point point = new Point(_pointsList.Count, position);
        _pointsList.Add(point);
        OnPointAdded?.Invoke(point);
    }

    public void ContinueLoop()
    {
        _continueLoop = true;
    }

    public void Execute()
    {
        if (_pointsList.Count == 0)
        {
            CreateRandomPoints();
        }

        FindConvexHull(_pointsList, _pointsList.Count);
        if (!_convexPolygon)
        {
            StartCoroutine(EarClipping());
        }
        else
        {
            StartCoroutine(TriangleFan());
        }
    }

    private IEnumerator TriangleFan()
    {
        Point averagePoint = new Point(0, Vector2.zero);

        foreach (Point point in _pointsList)
        {
            averagePoint.Position += point.Position;
        }

        averagePoint.Position /= _pointsList.Count;

        OnPointAdded?.Invoke(averagePoint);

        for (int i = 0; i < _pointsList.Count; i++)
        {
            yield return new WaitUntil(() => _continueLoop);
            _continueLoop = false;

            Point currentPoint = _pointsList[i];

            Point nextPoint = i == _pointsList.Count - 1 ? _pointsList[0] : _pointsList[i + 1];

            Triangle triangle = new Triangle(averagePoint, currentPoint, nextPoint);

            _trianglesList.Add(triangle);
            OnTriangleAdded?.Invoke(triangle);
        }
    }

    private void CreateRandomPoints()
    {
        for (int i = 0; i < _numberOfPoints; i++)
        {
            Point point = new Point(i,
                new Vector2(Random.Range(-_pointsRange, _pointsRange), Random.Range(-_pointsRange, _pointsRange)));
            _pointsList.Add(point);
            OnPointAdded?.Invoke(point);
        }
    }

    private void SortPointsList(List<Point> points, int numberOfPoints)
    {
        float yMin = points[0].Position.y;
        int minIndex = 0;

        for (int i = 1; i < numberOfPoints; i++)
        {
            float y = points[i].Position.y;

            if (y < yMin || yMin == y && points[i].Position.x < points[minIndex].Position.x)
            {
                yMin = points[i].Position.y;
                minIndex = i;
            }
        }

        Utilities.Swap(0, minIndex, points);

        P0 = points[0];

        PointComparer pointComparer = new PointComparer();
        points.Sort(1, numberOfPoints - 1, pointComparer);

        for (int i = 0; i < points.Count; i++)
        {
            points[i].Index = i;
            OnPointIndexed?.Invoke(points[i]);
        }

        OnPointsSorted?.Invoke(points);
    }

    private void FindConvexHull(List<Point> points, int numberOfPoints)
    {
        SortPointsList(points, numberOfPoints);
        List<Point> modifiedList = new List<Point>();
        modifiedList.AddRange(points);

        int modifiedArraySize = 1;
        for (int i = 1; i < numberOfPoints; i++)
        {
            //Only Updates Array If The Point Has a Different Orientation
            while (i < numberOfPoints - 1 && Utilities.GetOrientation(P0.Position, modifiedList[i].Position, modifiedList[i + 1].Position) == Orientation.COLLINEAR)
            {
                i++;
            }

            modifiedList[modifiedArraySize] = modifiedList[i];
            modifiedArraySize++;
        }

        if (modifiedArraySize < 3) return;

        Stack<Point> pointsInHull = new Stack<Point>();
        pointsInHull.Push(modifiedList[0]);
        pointsInHull.Push(modifiedList[1]);
        pointsInHull.Push(modifiedList[2]);

        _convexPolygon = true;
        for (int i = 3; i < modifiedArraySize; i++)
        {
            // Keep removing top while the angle formed by
            // points next-to-top, top, and points[i] makes
            // a non-left turn
            while (pointsInHull.Count > 1 &&
                   Utilities.GetOrientation(Utilities.NextToTop(pointsInHull).Position, pointsInHull.Peek().Position, modifiedList[i].Position) != Orientation.COUNTER_CLOCKWISE)
            {
                pointsInHull.Pop();
                _convexPolygon = false;
            }

            pointsInHull.Push(modifiedList[i]);
        }

        List<Point> convexPoints = pointsInHull.ToList();
        convexPoints.Reverse();
        _convexPoints.AddRange(convexPoints);
        OnPointsAddedToConvexList?.Invoke(convexPoints);
    }

    private void FindInitialEarTips()
    {
        //Loop para achar as orelhas
        foreach (Point currentPoint in _convexPoints)
        {
            //armazena ponto anterior
            Point previousPoint;
            if (currentPoint.Index == 0)
            {
                previousPoint = _pointsList[_pointsList.Count - 1];
            }
            else
            {
                previousPoint = _pointsList[currentPoint.Index - 1];
            }

            //Armazena próximo ponto
            Point nextPoint;
            if (currentPoint.Index == _pointsList.Count - 1)
            {
                nextPoint = _pointsList[0];
            }
            else
            {
                nextPoint = _pointsList[currentPoint.Index + 1];
            }

            //Cria um triangulo com os três pontos
            Triangle triangle = new Triangle(previousPoint, currentPoint, nextPoint);

            //se houver pontos dentro do triangulo pula o loop
            if (AnyPointInsideTriangle(triangle))
            {
                continue;
            }

            //se não houver, adiciona a lista de eartips
            _earTipPoints.Add(currentPoint);
            OnPointAddedToEartipList?.Invoke(currentPoint);
        }
    }

    private IEnumerator EarClipping()
    {
        //Método que acha as earTipsInicials
        FindInitialEarTips();

        //cria uma lista dos pontos que não foram checados ainda
        List<Point> uncheckedPoints = new List<Point>();
        uncheckedPoints.AddRange(_pointsList);

        int safety = 0;

        foreach (Point point in _earTipPoints)
        {
            Debug.Log(point.Index);
        }

        //percorre as lista das pontas de orelHa
        for (int i = 0; i < _earTipPoints.Count; i++)
        {
            safety++;
            if (safety > 1000) break;
            if (uncheckedPoints.Count < 3) break;

            yield return new WaitUntil(() => _continueLoop);
            _continueLoop = false;

            Debug.Log("------------------------------------");
            //armazena o ponto atual
            Point currentPoint = _earTipPoints[i];
            Debug.Log("currentPoint : " + currentPoint.Index);

            //método que busca o ponto anterior e o o próximo na lista
            GetSurroundingPoints(currentPoint, uncheckedPoints, out Point nextPoint, out Point previousPoint);
            Debug.Log("nextPoint : " + nextPoint.Index);
            Debug.Log("previousPoint : " + previousPoint.Index);

            //cria um triangulo com os tres pontos
            Triangle triangle = new Triangle(currentPoint, previousPoint, nextPoint);


            if(AnyPointInsideTriangle(triangle))
            {
                continue;
            }

            //remove o ponto atual da lista dos não checados
            uncheckedPoints.Remove(currentPoint);

            //adiciona o triangulo na lista dos triangulos
            _trianglesList.Add(triangle);
            OnTriangleAdded?.Invoke(triangle);

            //analisa se o ponto anterior virou a ponta de uma orelha
            Point analisedPoint = previousPoint;
            Debug.Log("ANALIZING PREVIOUS POINT : " + analisedPoint.Index);
            GetSurroundingPoints(analisedPoint, uncheckedPoints, out Point nextToAnalised, out Point previousToAnalised);

            Debug.Log("nextToAnalised : " + nextToAnalised.Index);
            Debug.Log("previousToAnalised : " + previousToAnalised.Index);

            if(uncheckedPoints.Contains(nextToAnalised) && uncheckedPoints.Contains(previousToAnalised))
            {
                CheckIfIsEartip(analisedPoint, previousToAnalised, nextToAnalised, i, true);
            }
            else
            {
                Debug.Log("SURROUNDING POINTS ALREADY REMOVED");
            }

            analisedPoint = nextPoint;
            Debug.Log("ANALIZING NEXT POINT : " + analisedPoint.Index);
            GetSurroundingPoints(analisedPoint, uncheckedPoints, out nextToAnalised, out previousToAnalised);
            Debug.Log("nextToAnalised : " + nextToAnalised.Index);
            Debug.Log("previousToAnalised : " + previousToAnalised.Index);

            if(uncheckedPoints.Contains(nextToAnalised) && uncheckedPoints.Contains(previousToAnalised))
            {
                CheckIfIsEartip(analisedPoint, previousToAnalised, nextToAnalised, i, false);
            }
            else
            {
                Debug.Log("SURROUNDING POINTS ALREADY REMOVED");
            }
        }

        Debug.Log("Ear Clipping complete");
    }

    private void CheckIfIsEartip(Point analisedPoint, Point previousPoint, Point nextPoint, int i, bool isPrevious)
    {
        Debug.Log("CHECKING IF IS AN EARTIP");
        if (_earTipPoints.Contains(analisedPoint))
        {
            Debug.Log("Point Already in eartip list");
            return;
        }

        if (Utilities.GetOrientation(analisedPoint.Position, previousPoint.Position, nextPoint.Position) ==
            Orientation.CLOCKWISE)
        {
            bool pointInsideTriangle = false;

            Debug.Log("Points ARE Convex");

            Triangle triangle = new Triangle(analisedPoint, previousPoint, nextPoint);
            pointInsideTriangle = AnyPointInsideTriangle(triangle);

            if (pointInsideTriangle)
            {
                return;
            }

            Debug.Log("FOUND NEW EARTIP: " + analisedPoint.Index);

            if (isPrevious)
            {
                _earTipPoints.Add(analisedPoint);
            }
            else
            {
                _earTipPoints.Insert(i + 1, analisedPoint);
            }

            OnPointAddedToEartipList?.Invoke(analisedPoint);
        }
        else
        {
            Debug.Log("Points ARE NOT Convex");
        }
    }

    private bool AnyPointInsideTriangle(Triangle triangle)
    {
        bool pointInsideTriangle = false;

        foreach (Point point in _pointsList)
        {
            if (point.Index == triangle.A.Index
                || point.Index == triangle.B.Index
                || point.Index == triangle.C.Index)
            {
                continue;
            }

            pointInsideTriangle = ThisPointInTriangle(point, triangle);

            if (pointInsideTriangle)
            {
                Debug.Log("THERE IS A POINT INSIDE THE TRIANGLE: " + point.Index);
                break;
            }
        }

        return pointInsideTriangle;
    }

    private void GetSurroundingPoints(Point currentPoint, List<Point> pointsList, out Point nextPoint, out Point previousPoint)
    {
        nextPoint = null;
        previousPoint = null;

        //percorre a lista de pontos não checados
        for (int i = 0; i < pointsList.Count; i++)
        {
            if (currentPoint == pointsList[i])
            {
                //quando achou o ponto atual armazena o próximo...
                if (i + 1 == pointsList.Count)
                {
                    nextPoint = pointsList[0];
                }
                else
                {
                    nextPoint = pointsList[i + 1];
                }

                //... e o anterior
                if (i - 1 < 0)
                {
                    previousPoint = pointsList[pointsList.Count - 1];
                }
                else
                {
                    previousPoint = pointsList[i - 1];
                }
            }
        }
    }

    float sign (Vector2 p1, Vector2 p2, Vector2 p3)
    {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    private bool ThisPointInTriangle(Point pt, Triangle triangle)
    {
        Point v1 = triangle.A;
        Point v2 = triangle.B;
        Point v3 = triangle.C;

        float d1 = sign(pt.Position, v1.Position, v2.Position);
        float d2 = sign(pt.Position, v2.Position, v3.Position);
        float d3 = sign(pt.Position, v3.Position, v1.Position);

        bool has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
        bool has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

        return !(has_neg && has_pos);
    }
}