﻿using System;
using UnityEngine;

[Serializable]
public class Point
{
    public int Index;
    public Vector2 Position;

    public Point(int index, Vector2 position)
    {
        Index = index;
        Position = position;
    }
}