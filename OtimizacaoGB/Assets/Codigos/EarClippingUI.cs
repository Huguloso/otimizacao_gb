using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EarClippingUI : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private EarClippingController _earClippingController;
    [SerializeField] private Image _pointPrefab;
    [SerializeField] private UILineRenderer _lineRendererPrefab;

    private readonly Dictionary<Point, Image> _pointUIDictionary = new Dictionary<Point, Image>();

    public void OnClick_Continue()
    {
        _earClippingController.ContinueLoop();
    }

    public void OnClick_Execute()
    {
        _earClippingController.Execute();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), eventData.position,
            eventData.pressEventCamera, out Vector2 localCursor);
        _earClippingController.CreatePoint(localCursor);
    }

    private void Awake()
    {
        _earClippingController.OnTriangleAdded += HandleTriangleAdded;
        _earClippingController.OnPointAdded += HandlePointAdded;
        _earClippingController.OnPointsAddedToConvexList += HandleConvexPointsAdded;
        _earClippingController.OnPointAddedToEartipList += HandleEartipAdded;
        _earClippingController.OnPointIndexed += HandlePointIndexed;
        _earClippingController.OnPointsSorted += HandlePointsSorted;
    }

    private void HandlePointIndexed(Point point)
    {
        _pointUIDictionary[point].GetComponentInChildren<TextMeshProUGUI>().text = point.Index.ToString();
        _pointUIDictionary[point].name = "POINT " + point.Index;
    }

    private void HandleEartipAdded(Point point)
    {
        Debug.Log("HandleEartipAdded : "+  point.Index);
        if (_pointUIDictionary.ContainsKey(point))
        {
            _pointUIDictionary[point].color = Color.green;
        }
        else
        {
            Debug.LogError("point isnt in dictionary");
        }
    }

    private void HandleConvexPointsAdded(List<Point> points)
    {
        foreach (Point point in points)
        {
            if (_pointUIDictionary.ContainsKey(point))
            {
                _pointUIDictionary[point].color = Color.yellow;
            }
            else
            {
                Debug.LogError("point isnt in dictionary");
            }
        }
    }

    private void HandlePointAdded(Point point)
    {
        Image pointImage = Instantiate(_pointPrefab, transform);
        pointImage.rectTransform.anchoredPosition = point.Position;
        pointImage.color = Color.black;

        _pointUIDictionary.Add(point, pointImage);
    }

    private void HandleTriangleAdded(Triangle triangle)
    {
        UILineRenderer lineRenderer = Instantiate(_lineRendererPrefab, transform);

        lineRenderer.color = Color.blue;

        lineRenderer.Points.Add(triangle.A.Position);
        lineRenderer.Points.Add(triangle.B.Position);
        lineRenderer.Points.Add(triangle.C.Position);
        lineRenderer.Points.Add(triangle.A.Position);

        if (_pointUIDictionary[triangle.A]!= null)
        {
            Destroy(_pointUIDictionary[triangle.A].gameObject);
        }
    }

    private void HandlePointsSorted(List<Point> points)
    {
        UILineRenderer lineRenderer = Instantiate(_lineRendererPrefab, transform);

        foreach (Point point in points)
        {
            lineRenderer.Points.Add(point.Position);
        }

        lineRenderer.Points.Add(_earClippingController.PointsList[0].Position);
        lineRenderer.transform.SetSiblingIndex(0);
    }

}
