﻿using System.Collections.Generic;
using UnityEngine;

public class PointComparer : IComparer<Point>
{
    public int Compare(Point point1, Point point2)
    {
        int orientation = (int)Utilities.GetOrientation(EarClippingController.P0.Position, point1.Position, point2.Position);

        if (orientation == 0)
        {
            return Vector2.Distance(EarClippingController.P0.Position, point2.Position) >= Vector2.Distance(EarClippingController.P0.Position, point1.Position) ? -1 : 1;
        }

        return (orientation == 2) ? -1 : 1;
    }
}