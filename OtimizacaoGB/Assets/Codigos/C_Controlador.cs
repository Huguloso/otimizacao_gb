﻿using System.Collections.Generic;
using UnityEngine;

public class C_Controlador : MonoBehaviour
{
    [Header("Definições_Dos_Pontos_Da_Malha")]
    public float Largura = 15;
    public float Altura = 15;
    private int NPontos = 16;

    public Material MatPonto;

    public bool WireframeMode = false;
    public Material MatPoligono;
    public Material MatWireframe;

    public GameObject MalhaGerada;

    public List<GameObject> Pontos = new List<GameObject>();

    [Header("Definições_Das_Variveis_De_Ordenação")]
    public List<GameObject> PontosAux = new List<GameObject>();

    //-----------------------------------------------------------//
    //------Variaveis_de_Controle_Do_Jogo------------------------//
    private bool MarcandoPontos = false;

    // Start is called before the first frame update
    void Start()
    {
        GerarPontosFixos();
    }

    // Update is called once per frame
    void Update()
    {
        Controles();
    }

    void Controles()
    {
        //Gerar Pontos Aleatorios
        if (Input.GetKeyDown(KeyCode.R))
        {
            PontosAleatorios();
        }
        //Gerar Pontos Predefinidos
        if (Input.GetKeyDown(KeyCode.F))
        {
            PontosPredefinidos();
        }
        //Testa Convexidade dos pontos
        if (Input.GetKeyDown(KeyCode.T))
        {
            VerificarConvex();
        }
        //Testa Criação de malha
        if (Input.GetKeyDown(KeyCode.Y))
        {
            bool Convex = VerificarConvex();
            PreparaMalha(PontosAux, Convex);
        }

        //Troca o modo de visão da malha
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if(MalhaGerada)
            {
                if(MalhaGerada.GetComponent<MeshRenderer>())
                {
                    if (!WireframeMode)
                    {
                        MalhaGerada.GetComponent<MeshRenderer>().material = MatWireframe;
                        WireframeMode = true;
                    }
                    else
                    {
                        MalhaGerada.GetComponent<MeshRenderer>().material = MatPoligono;
                        WireframeMode = false;
                    }
                }
            }
        }

        //Selecionar/Criar Pontos
        if (Input.GetMouseButtonDown(0))
        {
            //Se estiver marcando pontos
            if (MarcandoPontos)
            {
                Vector3 Pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GerarPonto(new Vector3(Pos.x, Pos.y, 0));
                NPontos++;
            }
        }
    }
    
    //-----------------------------------------------------------//

    void GerarPonto(Vector3 PosPonto)
    {
        //Cria a esfera representante do ponto
        Pontos.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
        //Define a nova posição do ponto
        Pontos[Pontos.Count - 1].transform.position = PosPonto;
        //Renomeia o ponto
        Pontos[Pontos.Count - 1].name = (Pontos.Count - 1).ToString();
        //Define o novo material do ponto
        Pontos[Pontos.Count - 1].GetComponent<MeshRenderer>().material = MatPonto;
    }

    void GerarPontosFixos()
    {
        //Define o numero de pontos Da cena
        NPontos = 10;
        //0
        GerarPonto(new Vector3(-16.87456f, -0.7527823f, 0));
        //1
        GerarPonto(new Vector3(-7.136882f, -8.654214f, 0));
        //2
        GerarPonto(new Vector3(2.1f, -0.3076307f, 0));
        //3
        GerarPonto(new Vector3(9.834501f, -5.426868f, 0));
        //4
        GerarPonto(new Vector3(17.17949f, 4.922893f, 0));
        //5
        GerarPonto(new Vector3(8.443403f, 3.921304f, 0));
        //6
        GerarPonto(new Vector3(4.41f, 12.22f, 0));
        //7
        GerarPonto(new Vector3(-3.534f, -1.767f, 0));
        //8
        GerarPonto(new Vector3(-12.31176f, 0.5826705f, 0));
        //9
        GerarPonto(new Vector3(-11.75533f, 9.59698f, 0));
    }

    void GerarPontosRand()
    {
        for (int P = 0; P < NPontos; P++)
        {
            float X, Y;
            X = Random.Range(-Largura, Largura);
            Y = Random.Range(-Altura, Altura);

            for (int PP = 0; PP < Pontos.Count; PP++)
            {
                while (Pontos[PP].transform.position == new Vector3(X, Y, 0))
                {
                    X = Random.Range(-Largura, Largura);
                    Y = Random.Range(-Altura, Altura);
                }
            }

            GerarPonto(new Vector3(X, Y, 0));
        }
    }

    void GerarMalha(List<Vector3> vert, List<Vector3Int> tris)
    {
        Vector3[] vertices = new Vector3[vert.Count];
        int[] triangles = new int[tris.Count * 3];

        //Converte Lista em Arrey
        for (int i = 0; i < vert.Count; i++)
        {
            vertices[i] = vert[i];
        }

        //Converte Lista em Arrey
        for (int i = 0; i < tris.Count; i++)
        {
            triangles[i * 3 + 0] = tris[i].x;
            triangles[i * 3 + 1] = tris[i].y;
            triangles[i * 3 + 2] = tris[i].z;
        }

        //Cria uma variavel para a malha
        Mesh mesh = new Mesh();

        //Definindo atributos da malha
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        if(MalhaGerada != null)
        {
            Destroy(MalhaGerada);
        }

        MalhaGerada = new GameObject("MalhaGerada", typeof(MeshFilter), typeof(MeshRenderer));
        MalhaGerada.transform.localScale = new Vector3(1, 1, 1);

        MalhaGerada.GetComponent<MeshFilter>().mesh = mesh;

        if (WireframeMode)
        {
            MalhaGerada.GetComponent<MeshRenderer>().material = MatWireframe;
        }
        else
        {
            MalhaGerada.GetComponent<MeshRenderer>().material = MatPoligono;
        }
    }

    //-----------------------------------------------------------//

    List<GameObject> OrdenarPontos(List<GameObject> PontosLista)
    {
        List<GameObject> PontosOrdenados = new List<GameObject>();

        PontosOrdenados.Add(PontosLista[0]);

        for (int P = 1; P < PontosLista.Count; P++)
        {
            //POrden.Add(Pontos[P]);
            //Se X do ultimo POrdenado for menor que o X do Ponto atual
            if (PontosOrdenados[PontosOrdenados.Count - 1].transform.position.x <= PontosLista[P].transform.position.x)
            {
                PontosOrdenados.Add(PontosLista[P]);
            }
            //Se o X do primeiro Ponto Ordenado for maior que  o X do Ponto atual
            else if (PontosOrdenados[0].transform.position.x >= PontosLista[P].transform.position.x)
            {
                PontosOrdenados.Insert(0, PontosLista[P]);
            }
            //Vai ter que procurar um numero maior
            else
            {
                for (int PO = 0; PO < PontosOrdenados.Count; PO++)
                {
                    if (PontosOrdenados[PO].transform.position.x >= PontosLista[P].transform.position.x)
                    {
                        PontosOrdenados.Insert(PO, PontosLista[P]);
                        PO = NPontos + 1;
                    }
                }
            }
        }

        return PontosOrdenados;
    }

    void LimparLista()
    {
        for (int P = 0; P < Pontos.Count; P++)
        {
            Destroy(Pontos[P]);
        }
        Pontos.Clear();
        PontosAux.Clear();
    }

    bool VerificarConvex()
    {
        //Ordena por ordem cresente de X do maio ao maior
        List<GameObject> PontosOrdenados = OrdenarPontos(Pontos);

        //GrahamScan para descobri convexidade
        List<GameObject> PontosGrahScan = new List<GameObject>();
        List<float> AngulosGrahScan = new List<float>();

        //Adiciona os primeiros valores
        PontosGrahScan.Add(PontosOrdenados[0]);
        AngulosGrahScan.Add(0);

        //Ordena um vetor de pontos em ordem de angulo
        for (int P = 1; P < NPontos; P++)
        {
            //Calcula o angulo Atual
            Vector2 Direcao = PontosOrdenados[0].transform.position - PontosOrdenados[P].transform.position;
            float Angulo = Vector2.Angle(-PontosOrdenados[0].transform.up, Direcao);

            //Se o Angulo do ultimo AnguloOrdenado for menor que o Angulo atual
            if (AngulosGrahScan[AngulosGrahScan.Count - 1] <= Angulo)
            {
                PontosGrahScan.Add(PontosOrdenados[P]);
                AngulosGrahScan.Add(Angulo);
            }
            //Se o Angulo do ultimo AnguloOrdenado for maior que o Angulo atual
            else if (AngulosGrahScan[0] >= Angulo)
            {
                PontosGrahScan.Add(PontosOrdenados[P]);
                AngulosGrahScan.Add(Angulo);
            }
            //Vai ter que procurar um numero maior
            else
            {
                for (int PO = 0; PO < PontosGrahScan.Count; PO++)
                {
                    if (AngulosGrahScan[PO] >= Angulo)
                    {
                        PontosGrahScan.Insert(PO, PontosOrdenados[P]);
                        AngulosGrahScan.Insert(PO, Angulo);
                        PO = NPontos + 1;
                    }
                }
            }
        }

        List<GameObject> PontosAngulados = new List<GameObject>();
        for (int i = 0; i < PontosGrahScan.Count; i++)
        {
            PontosAngulados.Add(PontosGrahScan[i]);
            PontosAux.Add(PontosGrahScan[i]);
        }

        PontosGrahScan.Clear();

        //---------Graham_Scan--------------------------------//

        //Inicia o vetor de GS
        PontosGrahScan.Add(PontosAngulados[0]);
        PontosGrahScan.Add(PontosAngulados[1]);
        int PontoAnterior = 1;

        //Descobrir a malha convexa
        for (int PontoAtual = 2; PontoAtual < PontosAngulados.Count; PontoAtual++)
        {
            //Calcula o angulo
            int Angulo = CCW(PontosGrahScan[PontoAnterior - 1].transform.position, PontosGrahScan[PontoAnterior].transform.position, PontosAngulados[PontoAtual].transform.position);

            //Caso ande no sentido horario
            if (Angulo <= 0)
            {
                //Adiciona o ponto atual no vetor
                PontosGrahScan.Add(PontosAngulados[PontoAtual]);
                //Atualiza o Ponto Atual
                PontoAnterior = PontoAtual;
            }
            //Caso não ande no sentido horario
            else
            {
                Debug.Log("Não é convexa!");

                return false;

                //PontosAngulados.RemoveAt(PontoAnterior);
                //PontosGrahScan.RemoveAt(PontoAnterior);
                //PontoAnterior -= 1;
                //PontoAtual -= 2;
            }
        }

        Debug.Log("É convexa!");

        return true;
    }

    void PreparaMalha(List<GameObject> PontosRecebidos, bool Convex)
    {
        List<Vector3> vert = new List<Vector3>();
        List<Vector3Int> tris = new List<Vector3Int>();

        //Se a malha for convexa
        if (Convex)
        {
            Vector3 PosMedia = new Vector3(0, 0, 0);

            for (int i = 0; i < Pontos.Count; i++)
            {
                PosMedia += Pontos[i].transform.position;
            }

            PosMedia = PosMedia / Pontos.Count;

            //Adiciona os veritices ao vetor
            for(int i = 0; i < PontosRecebidos.Count; i++)
            {
                vert.Add(PontosRecebidos[i].transform.position);
            }
            //Adiciona o ponto medio
            vert.Add(PosMedia);

            //Adicioa os Trigangulos a um vetor
            for (int i = 0; i < PontosRecebidos.Count - 1; i++)
            {
                tris.Add(new Vector3Int(i, i + 1, PontosRecebidos.Count));
            }

            //Fecha o poligono evitando Pacman
            tris.Add(new Vector3Int(PontosRecebidos.Count -1, 0, PontosRecebidos.Count));

            Debug.Log("Pode gerar malha");

            GerarMalha(vert, tris);

        }
        else
        {
            //---------Graham_Scan--------------------------------//
            List<GameObject> PontosGrahScan = new List<GameObject>();
            PontosGrahScan.Add(PontosRecebidos[0]);
            PontosGrahScan.Add(PontosRecebidos[1]);
            int PontoAnterior = 1;

            //Descobrir a malha convexa
            for (int PontoAtual = 2; PontoAtual < PontosRecebidos.Count; PontoAtual++)
            {
                //Calcula o angulo
                int Angulo = CCW(PontosGrahScan[PontoAnterior - 1].transform.position, PontosGrahScan[PontoAnterior].transform.position, PontosRecebidos[PontoAtual].transform.position);

                //Caso ande no sentido horario
                if (Angulo <= 0)
                {
                    //Adiciona o ponto atual no vetor
                    PontosGrahScan.Add(PontosRecebidos[PontoAtual]);
                    //Atualiza o Ponto Atual
                    PontoAnterior = PontoAtual;
                }
                //Caso não ande no sentido horario
                else
                {
                    PontosRecebidos.RemoveAt(PontoAnterior);
                    PontosGrahScan.RemoveAt(PontoAnterior);

                    PontoAnterior -= 1;
                    PontoAtual -= 2;
                }
            }

            //Adiciona os pontos Convexo em um vetor para armazana-los
            List<int> vertConvex = new List<int>();
            for (int i = 0; i < PontosGrahScan.Count; i++)
            {
                vertConvex.Add(int.Parse(PontosGrahScan[i].name));
            }

            //-----Organiza_vertConvex_em_ordem_crecente----------//
            List<int> ListaOrganizada = new List<int>();

            ListaOrganizada.Add(vertConvex[0]);

            for (int P = 1; P < vertConvex.Count; P++)
            {
                if (ListaOrganizada[ListaOrganizada.Count - 1] <= vertConvex[P])
                {
                    ListaOrganizada.Add(vertConvex[P]);
                }
                else if (ListaOrganizada[0] >= vertConvex[P])
                {
                    ListaOrganizada.Insert(0, vertConvex[P]);
                }
                //Vai ter que procurar um numero maior
                else
                {
                    for (int PO = 0; PO < ListaOrganizada.Count; PO++)
                    {
                        if (ListaOrganizada[PO] >= vertConvex[P])
                        {
                            ListaOrganizada.Insert(PO, vertConvex[P]);
                            PO = NPontos + 1;
                        }
                    }
                }
            }
            //----------------------------------------------------//

            EarClipping(Pontos, ListaOrganizada);
        }
    }

    int CCW(Vector2 a, Vector2 b, Vector2 c)
    {
        float area = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
        if (area < 0) return -1;//Sentido Horario
        else if (area > 0) return 1; //Sentido Anti-Horario
        else return 0;
    }

    bool PointInTriangle(Vector2 PontoTestado, Vector2 Vertice1, Vector2 Vertice2, Vector2 Vertice3)
    {
        float d1, d2, d3;
     
        d1 = CCW(PontoTestado, Vertice1, Vertice2);
        d2 = CCW(PontoTestado, Vertice2, Vertice3);
        d3 = CCW(PontoTestado, Vertice3, Vertice1);

        bool has_neg, has_pos;

        d1 = CCW(PontoTestado, Vertice1, Vertice2);
        d2 = CCW(PontoTestado, Vertice2, Vertice3);
        d3 = CCW(PontoTestado, Vertice3, Vertice1);

        has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
        has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

        return !(has_neg && has_pos);
    }

    void EarClipping(List<GameObject> PontosLista, List<int> vertConvex)
    {
        List<Vector3> vert = new List<Vector3>();
        List<Vector3Int> tris = new List<Vector3Int>();

        List<int> vertConcav = new List<int>();
        List<int> vertOrelha = new List<int>();
        List<int> vertexList = new List<int>();

        List<int> ListaAux = new List<int>();

        //Passando os valores de Convex para ListaAux
        for(int i = 0; i < vertConvex.Count; i++)
        {
            ListaAux.Add(vertConvex[i]);
        }

        //Para descobrir se o vertice é orelha ou é Concavo
        for (int i = 0; i < PontosLista.Count; i++)
        {
            vertexList.Add(int.Parse(PontosLista[i].name));

            if (ListaAux.Count > 0)
            {
                //Faz parte dos convexos pode ser orelha
                if (i == ListaAux[0])
                {
                    Vector3Int vertices = new Vector3Int(i - 1, i, i + 1);

                    if (i - 1 < 0)
                    {
                        vertices.x = PontosLista.Count - 1;
                    }
                    if (i + 1 >= PontosLista.Count)
                    {
                        vertices.z = 0;
                    }

                    Vector3 P1 = PontosLista[vertices.x].transform.position;
                    Vector3 P2 = PontosLista[vertices.y].transform.position;
                    Vector3 P3 = PontosLista[vertices.z].transform.position;

                    //Debug.Log($"{vertices.x}, {vertices.y}, {vertices.z}");

                    //Testa se tem algum ponto dentro do triangulo formado
                    for (int testarPontos = 0; testarPontos < PontosLista.Count; testarPontos++)
                    {
                        if (testarPontos != vertices.x && testarPontos != vertices.y && testarPontos != vertices.z)
                        {
                            bool PontoPertence = PointInTriangle(PontosLista[testarPontos].transform.position, P1, P2, P3);

                            if (PontoPertence)
                            {
                                //Debug.Log($"Achou o ponto {testarPontos}");
                                //Achou um ponto não é orelha
                                testarPontos = PontosLista.Count + 10;
                            }
                        }
                        //Não achou nenhum ponto dentro do triangulo, é orelha
                        if (testarPontos == PontosLista.Count - 1)
                        {
                            vertOrelha.Add(int.Parse(PontosLista[i].name));
                        }
                    }

                    ListaAux.RemoveAt(0);
                }
                //Não faz parte dos convexos
                else
                {
                    vertConcav.Add(int.Parse(PontosLista[i].name));
                }
            }
            else
            {
                vertConcav.Add(int.Parse(PontosLista[i].name));
            }
        }

        //-----Escreve_Os_Resultados--------------------------//
        EscreverDados(vertexList, vertConvex, vertConcav, vertOrelha);

        int LoopAtu = 0;
        while (vertOrelha.Count > 0)
        {
            Debug.Log($"//-----Loop_{LoopAtu}_iniciado--------------------------------//");
            LoopAtu++;
            //Passo 1:Remover Orelha
            //Cria uma variavel vertices para armazenar os vertices que compõe o triangulo
            Vector3Int vertxPos = new Vector3Int(1,2,3);

            //Descobre a posição da orelha na lista vertexList
            for (int i = 0; i < vertexList.Count; i++)
            {
                if (vertexList[i] == vertOrelha[0])
                {
                    vertxPos = new Vector3Int(i - 1, i, i + 1);
                    i = vertexList.Count + 1;
                    continue;
                }
            }

            //-----Corrige_os_Valores_de_vertxPos-----------------//
            if (vertxPos.x < 0)
            {
                vertxPos.x = vertexList.Count + vertxPos.x;
            }
            if (vertxPos.z >= vertexList.Count)
            {
                vertxPos.z = vertxPos.z - vertexList.Count;
            }
            
            //----------------------------------------------------//

            //Debug.Log($"vertxPos = {vertxPos}");
            //Debug.Log($"vertexList.Count = {vertexList.Count}");
            //Imprime os tres vertex que serão usados
            Debug.Log($"({vertexList[vertxPos.x]}, {vertexList[vertxPos.y]}, {vertexList[vertxPos.z]})");

            //Adiciona os vertices a lista de vertices
            vert.Add(PontosLista[vertexList[vertxPos.x]].transform.position);
            vert.Add(PontosLista[vertexList[vertxPos.y]].transform.position);
            vert.Add(PontosLista[vertexList[vertxPos.z]].transform.position);
            //Adiciona o triangulo a lista de triangulos
            tris.Add(new Vector3Int(tris.Count * 3 +2, tris.Count * 3 +1, tris.Count * 3 +0));

            //-----Remover_o_Vertex-------------------------------//
            Debug.Log($"Vertex que sera removido: {vertexList[vertxPos.y]}");
            //Remover da Lista de Orelhas   E
            vertOrelha.RemoveAt(0);
            //Remover da Lista de Concavos  R 
            for (int i = 0; i < vertConcav.Count; i++)
            {
                if (vertConcav[i] == vertexList[vertxPos.y])
                {
                    vertConcav.RemoveAt(i);
                }
            }
            //Remover da Lista de Convexos  C
            for (int i = 0; i < vertConvex.Count; i++)
            {
                if (vertConvex[i] == vertexList[vertxPos.y])
                {
                    vertConvex.RemoveAt(i);
                }
            }
            //Remover da Lista de Vertex    V
            vertexList.RemoveAt(vertxPos.y);

            //EscreverDados(vertexList, vertConvex, vertConcav, vertOrelha);

            //-----Corrige_os_Valores_de_vertxPos-----------------//
            //Debug.Log($"Antes de corrigir vertxPos = {vertxPos}");
            if (vertxPos.y >= vertexList.Count)
            {
                vertxPos.y = vertxPos.y - vertexList.Count;
            }
            else if (vertxPos.y < 0)
            {
                vertxPos.y = vertexList.Count + vertxPos.y;
            }
            
            //X
            if (vertxPos.x < 0)
            {
                vertxPos.x = vertexList.Count + vertxPos.x;
            }
            else if (vertxPos.x >= vertexList.Count)
            {
                vertxPos.x--;
            }
            else if(vertxPos.x == vertxPos.y)
            {
                vertxPos.x--;
            }
            //Z
            if (vertxPos.z >= vertexList.Count)
            {
                vertxPos.z = vertxPos.z - vertexList.Count;
                if (vertxPos.z == vertxPos.y)
                {
                    vertxPos.z++;
                }
            }
            else if(vertxPos.x < 0)
            {
                vertxPos.z = vertexList.Count + vertxPos.z;
                if (vertxPos.z == vertxPos.y)
                {
                    vertxPos.z--;
                }
            }
            else if (vertxPos.z == vertxPos.y)
            {
                vertxPos.z++;
            }

            //Debug.Log($"Depois de corrigir vertxPos = {vertxPos}");
            //Debug.Log($"vertexList.Count = {vertexList.Count}");

            //----------------------------------------------------//

            //Debug.Log($"vertxPos = {vertxPos}");
            //Debug.Log($"vertexList.Count = {vertexList.Count}");

            //Passo 2: Verificar se houve formação de orelhas
            //Se for o ultimo triangulo
            if (vertexList.Count == 3)
            {
                Debug.Log($"Esse é o ultimo poligono");
                vert.Add(PontosLista[vertexList[0]].transform.position);
                vert.Add(PontosLista[vertexList[1]].transform.position);
                vert.Add(PontosLista[vertexList[2]].transform.position);

                tris.Add(new Vector3Int(tris.Count * 3 + 2, tris.Count * 3 + 1, tris.Count * 3 + 0));

                vertOrelha.Clear();
                vertexList.Clear();
                continue;
            }

            //Cria variavel para amazenar os valores dos pontos do novo triangulo formado
            Vector3 P1 = PontosLista[vertexList[vertxPos.x]].transform.position;
            Vector3 P2 = PontosLista[vertexList[vertxPos.y]].transform.position;
            Vector3 P3 = PontosLista[vertexList[vertxPos.z]].transform.position;

            //----------------------------------------------------//
            //-----!!!Verificar_Para_Os_X_e_Y_Verticis!!!---------//
            //----------------------------------------------------//
            Debug.Log($"//-------------------//");
            //-----Vertice_Y--------------------------------------//
            Debug.Log($"Testes Y = ({vertexList[vertxPos.x]},{vertexList[vertxPos.y]},{vertexList[vertxPos.z]})");
            //Testa se o triangulo formado é concavo
            if (CCW(P1, P2, P3) <= 0)
            {
                //Debug.Log("Concavo não a necessidade de continuar o Loop");
                Debug.Log($"{vertexList[vertxPos.y]} é concavo");
                
                //-----Insere_na_Lista_de_Concavos--------------------//
                bool NaLista = false;
                for (int i = 0; i < vertConcav.Count; i++)
                {
                    if (vertConcav[i] == vertexList[vertxPos.y])
                    {
                        NaLista = true;
                        i = vertConcav.Count + 1;
                    }
                }
                if (!NaLista)
                {
                    vertConcav.Insert(0, vertexList[vertxPos.y]);
                }

                //-----Remove_da_Lista_de_Convexos--------------------//
                for (int i = 0; i < vertConvex.Count; i++)
                {
                    //Caso seja menor que o a posição testada atualmente
                    if (vertConvex[i] == vertexList[vertxPos.y])
                    {
                        vertConvex.RemoveAt(i);
                        i = vertConvex.Count + 1;
                    }
                }

                //-----Remove_da_Lista_de_Orelhas---------------------//
                for (int i = 0; i < vertOrelha.Count; i++)
                {
                    //Caso seja menor que o a posição testada atualmente
                    if (vertOrelha[i] == vertexList[vertxPos.y])
                    {
                        vertOrelha.RemoveAt(i);
                        i = vertOrelha.Count + 1;
                    }
                }
            }
            //O Triangulo formado é Convexo
            else
            {
                //Debug.Log($"Começamos a testar o {vertexList[vertxPos.y]} nas Listas");
                Debug.Log($"{vertexList[vertxPos.y]} é convexo");
                //-----Testa_se_o_Vertex_é_Concavo--------------------//
                for (int i = 0; i < vertConcav.Count; i++)
                {
                    //Caso ache o vertex na lista
                    if (vertConcav[i] == vertexList[vertxPos.y])
                    {
                        //Remove da lista
                        vertConcav.RemoveAt(i);
                        i = vertConcav.Count + 1;
                    }
                }

                bool NaLista = false;
                //-----Insere_na_Lista_de_Convexos--------------------//
                for (int i = 0; i < vertConvex.Count; i++)
                {
                    if (vertConvex[i] == vertexList[vertxPos.y])
                    {
                        NaLista = true;
                        i = vertConvex.Count + 1;
                    }
                }
                //Insere no Inicio da Lista
                if (!NaLista)
                {
                    vertConvex.Insert(0, vertexList[vertxPos.y]);
                }

                //-----Verificar_se_é_Orelha--------------------------//
                //Verificar ser tem um ponto dentro do triangulo formado
                bool achouPonto = false;

                for (int PA = 0; PA < vertexList.Count; PA++)
                {
                    //PA = Ponto Atual
                    //Caso o PA não seja um dos pontos do triangulo
                    if (vertxPos.x != PA && vertxPos.y != PA && vertxPos.z != PA)
                    {
                        //Cria uma variavel para varificar se o ponto pertence ao tri
                        achouPonto = PointInTriangle(PontosLista[vertexList[PA]].transform.position, P1, P2, P3);
                        //Caso ache um ponto pertencente ao ponto
                        if (achouPonto)
                        {
                            //Achou um ponto não é orelha
                            PA = vertexList.Count + 1;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!achouPonto)
                {
                    //Não achou nenhum ponto dentro do triangulo, é uma orelha
                    //-----Insere_na_Lista_de_Orelhas---------------------//
                    Debug.Log($"{vertexList[vertxPos.y]} é orelha");
                    NaLista = false;
                    for (int i = 0; i < vertOrelha.Count; i++)
                    {
                        //Ja ta na lista
                        if (vertOrelha[i] == vertexList[vertxPos.y])
                        {
                            NaLista = true;
                            i = vertOrelha.Count + 1;
                        }

                    }

                    if(vertOrelha.Count == 0)
                    {
                        vertOrelha.Add(vertexList[vertxPos.y]);
                    }
                    else if(!NaLista && vertOrelha.Count > 0)
                    {
                        vertOrelha.Insert(0,vertexList[vertxPos.y]);
                    }
                }
            }

            Debug.Log($"vertxPos = {vertxPos}");
            //-----Vertice_X--------------------------------------//
            //-----Corrige_os_Valores_de_vertxPos-----------------//
            //Testa o vertex anterior
            vertxPos = vertxPos - new Vector3Int(1,1,1);
            //Corrige as quebras
            for(int i = 0; i < 3; i++)
            {
                //Caso passe do maximo
                if (vertxPos[i] >= vertexList.Count)
                {
                    vertxPos[i] = vertexList.Count + vertxPos[i];
                }
                //Caso passe do minimo
                else if(vertxPos[i] < 0)
                {
                    vertxPos[i] = vertexList.Count + vertxPos[i];
                }

            }

            P1 = PontosLista[vertexList[vertxPos.x]].transform.position;
            P2 = PontosLista[vertexList[vertxPos.y]].transform.position;
            P3 = PontosLista[vertexList[vertxPos.z]].transform.position;

            Debug.Log($"Testes X = ({vertexList[vertxPos.x]},{vertexList[vertxPos.y]},{vertexList[vertxPos.z]})");
            //Testa se o triangulo formado é concavo
            if (CCW(P1, P2, P3) <= 0)
            {
                //Debug.Log("Concavo não a necessidade de continuar o Loop");
                Debug.Log($"{vertexList[vertxPos.y]} é concavo");
                //-----Insere_na_Lista_de_Concavos--------------------//
                bool NaLista = false;
                for (int i = 0; i < vertConcav.Count; i++)
                {
                    if (vertConcav[i] == vertexList[vertxPos.y])
                    {
                        NaLista = true;
                        i = vertConcav.Count + 1;
                    }
                }
                if (!NaLista)
                {
                    vertConcav.Add(vertexList[vertxPos.y]);
                }

                //-----Remove_da_Lista_de_Convexos--------------------//
                for (int i = 0; i < vertConvex.Count; i++)
                {
                    //Caso seja menor que o a posição testada atualmente
                    if (vertConvex[i] == vertexList[vertxPos.y])
                    {
                        vertConvex.RemoveAt(i);
                        i = vertConvex.Count + 1;
                    }
                }

                //-----Remove_da_Lista_de_Orelhas---------------------//
                for (int i = 0; i < vertOrelha.Count; i++)
                {
                    //Caso seja menor que o a posição testada atualmente
                    if (vertOrelha[i] == vertexList[vertxPos.y])
                    {
                        vertOrelha.RemoveAt(i);
                        i = vertOrelha.Count + 1;
                    }
                }
            }
            //O Triangulo formado é Convexo
            else
            {
                //Debug.Log($"Começamos a testar o {vertexList[vertxPos.y]} nas Listas");
                Debug.Log($"{vertexList[vertxPos.y]} é convexo");
                //-----Testa_se_o_Vertex_é_Concavo--------------------//
                for (int i = 0; i < vertConcav.Count; i++)
                {
                    //Caso ache o vertex na lista
                    if (vertConcav[i] == vertexList[vertxPos.y])
                    {
                        //Remove da lista
                        vertConcav.RemoveAt(i);
                        i = vertConcav.Count + 1;
                    }
                }

                bool NaLista = false;
                //-----Insere_na_Lista_de_Convexos--------------------//
                for (int i = 0; i < vertConvex.Count; i++)
                {
                    if (vertConvex[i] == vertexList[vertxPos.y])
                    {
                        NaLista = true;
                        i = vertConvex.Count + 1;
                    }
                }
                //Insere no Inicio da Lista
                if (!NaLista)
                {
                    vertConvex.Add(vertexList[vertxPos.y]);
                }

                //-----Verificar_se_é_Orelha--------------------------//
                //Verificar ser tem um ponto dentro do triangulo formado
                bool achouPonto = false;

                for (int PA = 0; PA < vertexList.Count; PA++)
                {
                    //PA = Ponto Atual
                    //Caso o PA não seja um dos pontos do triangulo
                    if (vertxPos.x != PA && vertxPos.y != PA && vertxPos.z != PA)
                    {
                        //Cria uma variavel para varificar se o ponto pertence ao tri
                        achouPonto = PointInTriangle(PontosLista[vertexList[PA]].transform.position, P1, P2, P3);
                        //Caso ache um ponto pertencente ao ponto
                        if (achouPonto)
                        {
                            //Achou um ponto não é orelha
                            PA = vertexList.Count + 1;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!achouPonto)
                {
                    //Não achou nenhum ponto dentro do triangulo, é uma orelha
                    //-----Insere_na_Lista_de_Orelhas---------------------//
                    Debug.Log($"{vertexList[vertxPos.y]} é orelha");
                    NaLista = false;
                    for (int i = 0; i < vertOrelha.Count; i++)
                    {
                        //Ja ta na lista
                        if (vertOrelha[i] == vertexList[vertxPos.y])
                        {
                            NaLista = true;
                            i = vertOrelha.Count + 1;
                        }

                    }

                    if (vertOrelha.Count == 0)
                    {
                        vertOrelha.Add(vertexList[vertxPos.y]);
                    }
                    else if (!NaLista && vertOrelha.Count > 0)
                    {
                        vertOrelha.Add(vertexList[vertxPos.y]);
                    }
                }
            }

            Debug.Log($"//-------------------//");

            //Debug.Log($"vertexList.Count = {vertexList.Count}");
            EscreverDados(vertexList, vertConvex, vertConcav, vertOrelha);
        }

        if(vertexList.Count <= 0)
        {
            Debug.Log("O EarClipping não conseguiu resolver a malha.");
        }

        GerarMalha(vert, tris);
    }

    void EscreverDados(List<int> vertexList, List<int> vertConvex, List<int> vertConcav, List<int> vertOrelha)
    {
        //Convexos
        string texto = "Vertex V = {";
        for (int i = 0; i < vertexList.Count; i++)
        {
            texto += vertexList[i] + ", ";
        }
        texto += "}";
        Debug.Log(texto);
        //Convexos
        texto = "Convexos C = {";
        for (int i = 0; i < vertConvex.Count; i++)
        {
            texto += vertConvex[i] + ", ";
        }
        texto += "}";
        Debug.Log(texto);
        //Concavos
        texto = "Concavos R = {";
        for (int i = 0; i < vertConcav.Count; i++)
        {
            texto += vertConcav[i] + ", ";
        }
        texto += "}";
        Debug.Log(texto);
        //Orelhas
        texto = "Orelhas E = {";
        for (int i = 0; i < vertOrelha.Count; i++)
        {
            texto += vertOrelha[i] + ", ";
        }
        texto += "}";
        Debug.Log(texto);
    }

    //------Metodos_Para_Os_Botões-------------------------------//

    public void PontosAleatorios()
    {
        if (MarcandoPontos)
        {
            MarcarPontos();
        }
        LimparLista();
        GerarPontosRand();
    }

    public void PontosPredefinidos()
    {
        if (MarcandoPontos)
        {
            MarcarPontos();
        }
        LimparLista();
        GerarPontosFixos();
    }

    public void MarcarPontos()
    {
        if (MarcandoPontos)
        {
            MarcandoPontos = false;

            Destroy(Pontos[NPontos-1]);
            Pontos.RemoveAt(NPontos - 1);
            NPontos--;
        }
        else
        {
            LimparLista();

            NPontos = 0;

            MarcandoPontos = true;
        }
    }
}