﻿using System;

[Serializable]
public class Triangle
{
    public Point A;
    public Point B;
    public Point C;

    public Triangle(Point a, Point b, Point c)
    {
        A = a;
        B = b;
        C = c;
    }
}