using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour
{
    public static void Swap(int index1, int index2, List<Point> pointsList)
    {
        Point temp = pointsList[index1];
        pointsList[index1] = pointsList[index2];
        pointsList[index2] = temp;
    }

    public static Orientation GetOrientation(Vector2 p, Vector2 q, Vector2 r)
    {
        float val = (q.y - p.y) * (r.x - q.x) -
                    (q.x - p.x) * (r.y - q.y);

        if (val == 0)
        {
            return Orientation.COLLINEAR;
        }

        return val > 0 ? Orientation.CLOCKWISE : Orientation.COUNTER_CLOCKWISE;
    }

    public static Point NextToTop(Stack<Point> s)
    {
        Point p = s.Peek();
        s.Pop();
        Point res = s.Peek();
        s.Push(p);
        return res;
    }
}

public enum Orientation
{
    COLLINEAR = 0,
    CLOCKWISE = 1,
    COUNTER_CLOCKWISE = 2
}